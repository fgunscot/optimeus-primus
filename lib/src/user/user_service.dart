import 'package:time_manager_app/src/user/user_controller.dart';

class UserService {
  UserService(this._controller);
  UserController _controller;
}

class DailyModel {
  DailyModel({required this.date});

  DateTime date;
}

class HourModel {
  HourModel({required this.time, required this.ticketsList});
  DateTime time;
  List<TicketModel> ticketsList;
}

class TicketModel {
  TicketModel({
    required this.title,
    required this.message,
  });
  String title;
  String message;
  // String id; // this should be made up of a collection of above ids
}
