import 'package:flutter/foundation.dart';
import 'package:time_manager_app/src/user/user_service.dart';

class UserController with ChangeNotifier {
  UserController() {
    _service = UserService(this);
  }

  late UserService _service;
}
