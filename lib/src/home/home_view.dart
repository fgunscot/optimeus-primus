import 'package:flutter/material.dart';

class HomeView extends StatelessWidget {
  static const routeName = '/home';
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: <Color>[
                Colors.purple,
                Colors.blue,
                Colors.indigo,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
